/**
 * @file
 * Integration Javascript for FluidBox Formatter.
 */
(function ($) {
  Drupal.behaviors.fluidbox = {
    attach: function (context, settings) {
      $('.fluidbox').fluidbox();
    }
  };
})(jQuery);
