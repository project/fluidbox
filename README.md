Provides a field formatter to display images in a fluidbox. 

Fluid box is a 'distraction free' lightbox. It provides an elegant mechanism that allows a different image to be loaded inside the lightbox than the one that is on the page. It resizes the images on the page intelligently, i.e portrait images aren't squashed and large keep their aspect ratio.

##Requirements##
- jQuery Update module (jQuery >1.7) - http://drupal.org/project/jquery_update
- Libraries API Module (7.2+)- http://drupal.org/project/libraries
- Images Loaded Javascript Library  (Packaged Version) - https://github.com/desandro/imagesloaded
- Fluidbox Javascript Plugin - https://github.com/terrymun/Fluidbox

##Instructions##

Download imagesloaded packaged javascript into sites/all/libraries/imagesloaded
Clone the fluidbox repo into sites/all/libraries/fluidbox
In the manage display page for the content type which contains the image field you'd like to put into a lightbox change the formatter to "Fluidbox"

##Drush Make##

A drush make file has been provided to more easily download the required libraries. In the drupal root run

drush make --no-core PATH_TO_MODULE/fluidbox.make -y

Where path to module is the path where you installed the fluidbox module
