core = 7.x
api = 2
;Libraries 
;Images Loaded
libraries[imagesloaded][download][type] = "get"
libraries[imagesloaded][download][url] = "http://imagesloaded.desandro.com/imagesloaded.pkgd.min.js"
libraries[imagesloaded][download][destination] = "libraries"
libraries[imagesloaded][download][directory_name] = "imagesloaded"

;Fluidbox
libraries[fluidbox][download][type] = "git"
libraries[fluidbox][download][url] = "https://github.com/terrymun/Fluidbox.git"
libraries[fluidbox][download][destination] = "libraries"
libraries[fluidbox][download][directory_name] = "fluidbox"
